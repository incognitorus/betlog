class SportAssociation < ActiveRecord::Base
  belongs_to :sport

  default_scope { order :title }
end
