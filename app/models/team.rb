class Team < ActiveRecord::Base
  has_many :team_associations, dependent: :destroy
  accepts_nested_attributes_for :team_associations, reject_if: :all_blank, allow_destroy: true
  
  belongs_to :championship

  default_scope { order :title }
end
