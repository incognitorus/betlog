class EventTypeOne < ActiveRecord::Base
  belongs_to :event
  has_many :coefficients
end
