class ChampionshipAssociation < ActiveRecord::Base
  belongs_to :championship
  default_scope { order :title }
end
