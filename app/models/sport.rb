class Sport < ActiveRecord::Base
  has_many :championships, dependent: :destroy
  accepts_nested_attributes_for :championships, reject_if: :all_blank, allow_destroy: true

  has_many :sport_associations, dependent: :destroy
  accepts_nested_attributes_for :sport_associations, reject_if: :all_blank, allow_destroy: true

  default_scope { order :title }
end
