class Championship < ActiveRecord::Base
  has_many :events
  belongs_to :sport

  has_many :championship_associations, dependent: :destroy
  accepts_nested_attributes_for :championship_associations, reject_if: :all_blank, allow_destroy: true

  has_many :teams, dependent: :destroy
  accepts_nested_attributes_for :teams, reject_if: :all_blank, allow_destroy: true

  default_scope { order :title }
end