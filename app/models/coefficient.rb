class Coefficient < ActiveRecord::Base
  belongs_to :bookmaker
  belongs_to :event_type_one

  def bookmaker_name
    bookmaker = Bookmaker.find(self.bookmaker_id)
    bookmaker[:name]
  end
end
