class TeamAssociation < ActiveRecord::Base
  belongs_to :team

  default_scope { order :title }
end
