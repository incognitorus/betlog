#= require jquery
#= require jquery_ujs

#= require vendor/jquery.placeholder.min
#= require vendor/jquery.bxslider.min
#= require vendor/jquery.fancybox.pack

#= require vendor/angular/angular.min
#= require vendor/angular/angular-resource.min
#= require vendor/angular/angular-cookies.min
#= require vendor/angular/angular-animate.min
#= require vendor/angular-ui-route.min
#= require angular-rails-templates


#= require vendor/hightchars/hightchars.js
#= require vendor/hightchars/exporting.js
#= require vendor/jcarousel/jquery.jcarousel.min.js


#= require main

#= require_tree ./templates
#= require_tree ./angular/modules
#= require_tree ./angular/services
#= require_tree ./angular/directives
#= require_tree ./angular/controllers
#= require angular/betlog