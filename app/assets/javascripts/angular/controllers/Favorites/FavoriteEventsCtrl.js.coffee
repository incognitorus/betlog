angular.module('betlog.favorites').controller 'FavoriteEvents', [
  '$scope'
  '$http'
  'filterFilter'
  '$animate'
  'Storage'
  ($scope, $http, filterFilter, $animate, Storage) ->
  
    $scope.coefficients = []
    $scope.events = []
    $scope.championships = []
    $scope.countries = []
    $scope.bookmakers = []
    $scope.sports = []
    $scope.favorite_championships = []
    $scope.events_of_championship = {}
    $scope.sport_of_championship = {}
    $scope.country_of_championship = {}
    $scope.championship_of_event = {}
    $scope.max_coefficient_of_event = {}
    $scope.coefficients_params_of_event = {}
    $scope.coefficients_dates_of_event = {}

    $scope.mainDataIsLoaded = ->
      if $scope.sports.length and $scope.championships.length and $scope.events.length and $scope.countries.length and $scope.coefficients.length and $scope.bookmakers.length
        return true
      return

    $scope.getBookmaker = (id) ->
      filterFilter($scope.bookmakers, { 'id': id }, true)[0]

    $scope.reloadCoefficients = ->
      $scope.coefficients = Storage.getCoefficients()
      return

    $scope.reloadEvents = ->
      $scope.events = Storage.getEvents()
      return

    $scope.reloadChampionships = ->
      $scope.championships = Storage.getChampionships()
      return

    $scope.reloadBookmakers = ->
      $scope.bookmakers = Storage.getBookmakers()
      return

    $scope.reloadSports = ->
      $scope.sports = Storage.getSports()
      return

    $scope.reloadCountries = ->
      $scope.countries = Storage.getCountries()
      return

    $scope.reloadFavoriteChampionships = ->
      if !$scope.mainDataIsLoaded()
        return false
      $scope.favorite_championships = []
      $scope.championships.map (championship) ->
        if championship.id == 1 or championship.id == 2 or championship.id == 3 or championship.id == 4 or championship.id == 5
          $scope.favorite_championships.push championship
          $scope.reloadEventsByChampionship championship
        return
      return

    $scope.reloadEventsByChampionship = (championship) ->
      $scope.events_of_championship[championship.id] = []
      filterFilter($scope.events, { championship_id: championship.id }, true).map (event, key) ->
        if key <= 3
          $scope.events_of_championship[championship.id].push event
          $scope.reloadSportByChampionship championship
          $scope.reloadCountryByChampionship championship
          $scope.reloadChampionshipByEvent championship
          $scope.reloadMaxCoefficientFirstByEvent event
          $scope.reloadMaxCoefficientSecondByEvent event
          $scope.reloadMaxCoefficientDrawByEvent event
        return
      return

    $scope.reloadSportByChampionship = (championship) ->
      if $scope.sports.length
        $scope.sport_of_championship[championship.id] = filterFilter($scope.sports, { id: championship.sport_id }, true)[0]
      return

    $scope.reloadCountryByChampionship = (championship) ->
      if $scope.countries.length
        $scope.country_of_championship[championship.id] = filterFilter($scope.countries, { id: championship.country_id }, true)[0]
      return

    $scope.reloadChampionshipByEvent = (event) ->
      if $scope.championships.length
        $scope.championship_of_event[event.id] = filterFilter($scope.championships, { id: event.championship_id }, true)[0]
      return

    $scope.reloadMaxCoefficientByEventAndName = (event, param) ->
      if !$scope.max_coefficient_of_event[event.id]
        $scope.max_coefficient_of_event[event.id] = {}
      $scope.max_coefficient_of_event[event.id][param] = filterFilter($scope.coefficients, { 'event_id': event.id }, true).sort((a, b) ->
        a = a[param]
        b = b[param]
        b - a
      )[0][param]
      return

    $scope.reloadMaxCoefficientFirstByEvent = (event) ->
      $scope.reloadMaxCoefficientByEventAndName event, 'first'
      return

    $scope.reloadMaxCoefficientDrawByEvent = (event) ->
      $scope.reloadMaxCoefficientByEventAndName event, 'draw'
      return

    $scope.reloadMaxCoefficientSecondByEvent = (event) ->
      $scope.reloadMaxCoefficientByEventAndName event, 'second'
      return

    $scope.reloadCoefficientsParamEvent = (coeff_item, param) ->
      if !$scope.coefficients_params_of_event[coeff_item.event_id]
        $scope.coefficients_params_of_event[coeff_item.event_id] = {}
      $scope.coefficients_params_of_event[coeff_item.event_id][param] = []
      coefficients = filterFilter($scope.coefficients, { 'event_id': coeff_item.event_id }, true)
      coefficients.map (coefficient) ->
        if $scope.coefficients_params_of_event[coeff_item.event_id][param].indexOf(coefficient.created_at) == -1
          $scope.coefficients_params_of_event[coeff_item.event_id][param].push coefficient[param]
        return
      return

    $scope.reloadCoefficientsDatesEvent = (coeff_item) ->
      coefficients = filterFilter($scope.coefficients, { 'event_id': coeff_item.event_id }, true)
      coefficients.map (coefficient) ->
        if $scope.coefficients_dates_of_event[coeff_item.event_id].indexOf(coefficient.created_at) == -1
          $scope.coefficients_dates_of_event[coeff_item.event_id].push $filter('date')(coefficient.created_at, 'dd.MM')
        return
      return

    $scope.reloadJcarousel = ->
      $('.jcarousel').jcarousel()
      return

    $scope.reloadSports()
    $scope.reloadCountries()
    $scope.reloadChampionships()
    $scope.reloadEvents()
    $scope.reloadCoefficients()
    $scope.reloadBookmakers()
    $scope.reloadFavoriteChampionships()


    $scope.$on 'sendFavoriteSports', (event, sports) ->
      $scope.favorite_sports = sports
      $scope.reloadFavoriteChampionships()
      return
    $scope.$on 'reloadCountries', ->
      $scope.reloadCountries()
      $scope.reloadFavoriteChampionships()
      return
    $scope.$on 'reloadSports', ->
      $scope.reloadSports()
      $scope.reloadFavoriteChampionships()
      return
    $scope.$on 'reloadChampionships', ->
      $scope.reloadChampionships()
      $scope.reloadFavoriteChampionships()
      return
    $scope.$on 'reloadEvents', ->
      $scope.reloadEvents()
      $scope.reloadFavoriteChampionships()
      return
    $scope.$on 'reloadCoefficients', ->
      $scope.reloadCoefficients()
      $scope.reloadFavoriteChampionships()
      return
    $scope.$on 'reloadBookmakers', ->
      $scope.reloadBookmakers()
      $scope.reloadFavoriteChampionships()
      return
]