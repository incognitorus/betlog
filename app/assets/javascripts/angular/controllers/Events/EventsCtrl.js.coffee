angular.module('betlog.events').controller 'Events', [
  '$scope'
  '$rootScope'
  '$http'
  'filterFilter'
  '$animate'
  ($scope, $rootScope, $http, filterFilter, $animate) ->
    $scope.active_championship = false
    $scope.active_sport = false
    $scope.active_country = false

    $scope.getEventsAllChampionships = ->
      result = []
      $scope.getChampionshipsBySportAndCountry().map (championship) ->
        championship_events = filterFilter($scope.getEvents(), { championship_id: championship.id }, true)
        championship_events.map (events) ->
          result.push events
          return
        return
      result

    $scope.getEventsByActiveChampionship = ->
      filterFilter $scope.getEvents(), { championship_id: $scope.getActiveChampionship().id }, true

    $scope.getChampionshipsBySportAndCountry = ->
      filterFilter $scope.getChampionships(), {
        sport_id: parseInt($routeParams.sportId)
        country_id: parseInt($routeParams.countryId)
      }, true

    $scope.addChampionshipInToManagerLeagues = ->
      Storage.addChampionshipInToManagerLeagues $scope.getActiveChampionship()
      return

    $scope.removeChampionshipFromManagerLeagues = ->
      Storage.removeChampionshipFromManagerLeagues $scope.getActiveChampionship()
      return

    $scope.getDatesFromCmapionshipEvents = ->
      result = []
      event_scope = if $scope.getActiveChampionship() then $scope.getEventsByActiveChampionship() else $scope.getEventsAllChampionships()
      event_scope.map (event, i) ->
        day = event.date_event.slice(0, 10)
        if !filterFilter(result, day).length
          result.push day
        return
      result.sort()

    $scope.toggle = (event_obj) ->
      event_obj.isOpened = if event_obj.isOpened then false else true
      return

    $scope.getCountryName = ->
      result_country = filterFilter($scope.getCountries(), { id: parseInt($routeParams.countryId) }, true)[0]
      if result_country
        return result_country.name
      return

    $scope.managerLeagueHasChampionship = (championship) ->
      if championship
        return if filterFilter(Storage.getManagedLeagues(), { id: championship.id }, true).length then true else false
      return

    $scope.getCountries = ->
      Storage.getCountries()

    $scope.getSports = ->
      Storage.getSports()

    $scope.getEvents = ->
      Storage.getEvents()

    $scope.getChampionships = ->
      Storage.getChampionships()

    ### on ###

    $scope.$on 'reloadCountries', ->
      #console.log(Storage.getCountries());
      Storage.getCountries()
      return
    $scope.$on 'reloadSports', ->
      Storage.getSports()
      return
    $scope.$on 'reloadChampionships', ->
      Storage.getChampionships()
      return
    $scope.$on 'reloadEvents', ->
      Storage.getEvents()
      return
    return
]