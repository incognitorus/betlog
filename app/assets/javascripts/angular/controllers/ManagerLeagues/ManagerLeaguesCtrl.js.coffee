angular.module('betlog.manager_leagues').controller 'ManagerLeague', [
  '$scope'
  '$rootScope'
  '$http'
  'filterFilter'
  '$animate'
  'Storage'
  ($scope, $rootScope, $http, filterFilter, $animate, Storage) ->
    $scope.active_sport = false

    $scope.getManagedLeagues = ->
      Storage.getManagedLeagues()

    $scope.getSports = ->
      Storage.getSports()

    $scope.getCountries = ->
      Storage.getCountries()

    $scope.toggle = (sport) ->
      $scope.active_sport = if $scope.active_sport then false else sport
      return

    $scope.getSportsManagedChampionships = ->
      sports = []
      result = []
      $scope.getManagedLeagues().map (championship) ->
        if sports.indexOf(championship.sport_id) == -1
          sports.push championship.sport_id
        return
      $scope.getSports().map (sport) ->
        if sports.indexOf(sport.id) != -1
          result.push sport
        return
      result

    $scope.getManagedChampionshipsBySport = (sport) ->
      filterFilter $scope.getManagedLeagues(), { sport_id: sport.id }, true

    $scope.getCountryByChampionship = (championship) ->
      filterFilter($scope.getCountries(), { id: championship.country_id }, true)[0]

    $scope.remove = (championship) ->
      Storage.removeChampionshipFromManagerLeagues championship
      return

    ###  on  ###

    $rootScope.$on 'reloadManagedLeagues', ->
      $scope.getManagedLeagues()
      return
    $rootScope.$on 'reloadSports', ->
      $scope.getSports()
      return
    $rootScope.$on 'reloadCountries', ->
      $scope.getCountries()
      return
    return
]