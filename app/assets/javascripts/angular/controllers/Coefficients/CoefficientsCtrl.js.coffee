angular.module('betlog.events').controller 'Coefficients', [
  '$scope'
  '$rootScope'
  '$http'
  'filterFilter'
  '$filter'
  '$animate'
  'Storage'
  ($scope, $rootScope, $http, filterFilter, $filter, $animate, Storage) ->
    $scope.coefficients = []
    $scope.grouped_coefficients_of_event = {}
    $scope.last_grouped_coefficients_of_event = {}
    $scope.coefficients_dates_event_one_bookmaker = {}
    $scope.coefficients_params_event_only_one_bookmaker = {}

    $scope.reloadCoefficients = ->
      $scope.coefficients = Storage.getCoefficients()
      return

    $scope.getLastGroupedCoefficientsByEvent = (event) ->
      if typeof $scope.last_grouped_coefficients_of_event[event.id] == 'undefined'
        $scope.reloadLastCoefficientsGroupedByBookmaker event
      $scope.last_grouped_coefficients_of_event[event.id]

    $scope.reloadLastCoefficientsGroupedByBookmaker = (event) ->
      $scope.reloadCoefficientsGroupedByBookmaker event
      $scope.last_grouped_coefficients_of_event[event.id] = []
      $scope.grouped_coefficients_of_event[event.id].map (coefficients_item) ->
        last_coefficients_item = coefficients_item[coefficients_item.length - 1]
        $scope.last_grouped_coefficients_of_event[event.id].push last_coefficients_item
        $scope.reloadCoefficientsDatesEventOnlyOneBookmaker last_coefficients_item
        $scope.reloadCoefficientsParamEventOnlyOneBookmaker last_coefficients_item, 'first'
        $scope.reloadCoefficientsParamEventOnlyOneBookmaker last_coefficients_item, 'draw'
        $scope.reloadCoefficientsParamEventOnlyOneBookmaker last_coefficients_item, 'second'
        $scope.reloadCoefficientsParamEventOnlyOneBookmaker last_coefficients_item, 'first_or_draw'
        $scope.reloadCoefficientsParamEventOnlyOneBookmaker last_coefficients_item, 'first_or_second'
        $scope.reloadCoefficientsParamEventOnlyOneBookmaker last_coefficients_item, 'draw_or_second'
        $scope.reloadCoefficientsParamEventOnlyOneBookmaker last_coefficients_item, 'coeff_first_fora'
        $scope.reloadCoefficientsParamEventOnlyOneBookmaker last_coefficients_item, 'coeff_second_fora'
        $scope.reloadCoefficientsParamEventOnlyOneBookmaker last_coefficients_item, 'coeff_first_total'
        $scope.reloadCoefficientsParamEventOnlyOneBookmaker last_coefficients_item, 'coeff_second_total'
        return
      return

    $scope.reloadCoefficientsGroupedByBookmaker = (event) ->
      bookmakers = []
      coefficients = filterFilter($scope.coefficients, { 'event_id': event.id }, true)
      $scope.grouped_coefficients_of_event[event.id] = []
      coefficients.map (coefficients_item) ->
        if bookmakers.indexOf(coefficients_item.bookmaker_name) == -1
          result_coeffs = filterFilter(coefficients, { 'bookmaker_name': coefficients_item.bookmaker_name }, true)
          $scope.grouped_coefficients_of_event[event.id].push result_coeffs
          bookmakers.push coefficients_item.bookmaker_name
        return
      return

    $scope.reloadCoefficientsDatesEventOnlyOneBookmaker = (coeff_item) ->
      coefficients = filterFilter($scope.coefficients, {
        'event_id': coeff_item.event_id
        'bookmaker_name': coeff_item.bookmaker_name
      }, true)
      if typeof $scope.coefficients_dates_event_one_bookmaker[coeff_item.id] == 'undefined'
        $scope.coefficients_dates_event_one_bookmaker[coeff_item.id] = []
      coefficients.map (coefficient) ->
        if $scope.coefficients_dates_event_one_bookmaker[coeff_item.id].indexOf(coefficient.created_at) == -1
          $scope.coefficients_dates_event_one_bookmaker[coeff_item.id].push $filter('date')(coefficient.created_at, 'dd.MM')
        return
      return

    $scope.reloadCoefficientsParamEventOnlyOneBookmaker = (coeff_item, param) ->
      coefficients = filterFilter($scope.coefficients, {
        'event_id': coeff_item.event_id
        'bookmaker_name': coeff_item.bookmaker_name
      }, true)
      if typeof $scope.coefficients_params_event_only_one_bookmaker[coeff_item.id] == 'undefined'
        $scope.coefficients_params_event_only_one_bookmaker[coeff_item.id] = {}
      $scope.coefficients_params_event_only_one_bookmaker[coeff_item.id][param] = []
      coefficients.map (coefficient) ->
        if $scope.coefficients_params_event_only_one_bookmaker[coeff_item.id][param].indexOf(coefficient.created_at) == -1
          $scope.coefficients_params_event_only_one_bookmaker[coeff_item.id][param].push coefficient[param]
        return
      return

    $scope.isLogged = ->
      Storage.hasCurrentUser()

    $scope.reloadCoefficients()

    ### on ###

    $rootScope.$on 'reloadCoefficients', ->
      $scope.reloadCoefficients()
      return
    return
]