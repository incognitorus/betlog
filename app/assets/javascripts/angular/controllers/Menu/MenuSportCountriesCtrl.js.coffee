angular.module('betlog.menu').controller 'MenuSportCountries', [
  '$scope'
  '$rootScope'
  '$http'
  '$element'
  'filterFilter'
  ($scope, $rootScope, $http, $element, filterFilter) ->
    $scope.active_country = false
    $scope.first_word = ''
    $scope.isActive = false
    column_height = 6

    $scope.getCountriesBySport = (sport) ->
      result = []
      championships = filterFilter($scope.getChampionships(), { sport_id: sport.id }, true)
      championships.map (championship) ->
        country = filterFilter($scope.getCountries(), { id: championship.country_id }, true)[0]
        if country and result.indexOf(country) == -1
          result.push country
        return
      result

    $scope.getBlockCountryBySport = (sport) ->
      countries_length = $scope.getCountriesBySport(sport).length
      count_result = if countries_length then Math.ceil(countries_length / column_height) else 0
      result = []
      i = 0
      while i < count_result
        result.push i
        i++
      result

    $scope.getCountriesByBlockAndSport = (block_number, sport) ->
      result = []
      $scope.getCountriesBySport(sport).map (country, key) ->
        if key >= block_number * column_height and key < block_number * column_height + column_height
          result.push country
        return
      result

    $scope.getFirstWordByCountry = (country) ->
      country.name.slice(0, 1).toUpperCase()

    $scope.setFirstWord = (word) ->
      $scope.first_word = word
      return

    $scope.getCountries = ->
      Storage.getCountries()

    $scope.getChampionships = ->
      Storage.getChampionships()

    ### on ###

    $scope.$on 'reloadCountries', ->
      $scope.getCountries()
      return
    $scope.$on 'reloadChampionships', ->
      $scope.getChampionships()
      return
    return
]