angular.module('betlog.next_events').controller 'NextEvents', [
  '$scope'
  '$http'
  'filterFilter'
  '$animate'
  ($scope, $http, filterFilter, $animate) ->
    $scope.events = []
    $scope.sports = []
    $scope.championships = []
    $scope.coefficients = []
    $scope.countries = []
    $scope.next_events = []
    $scope.sport_of_championship = {}
    $scope.country_of_championship = {}
    $scope.championship_of_event = {}

    $scope.mainDataIsLoaded = ->
      if $scope.sports.length and $scope.championships.length and $scope.events.length and $scope.countries.length and $scope.coefficients.length
        return true
      return

    $scope.reloadCoefficients = ->
      $scope.coefficients = Storage.getCoefficients()
      return

    $scope.reloadEvents = ->
      $scope.events = Storage.getEvents()
      return

    $scope.reloadChampionships = ->
      $scope.championships = Storage.getChampionships()
      return

    $scope.reloadSports = ->
      $scope.sports = Storage.getSports()
      return

    $scope.reloadCountries = ->
      $scope.countries = Storage.getCountries()
      return

    $scope.reloadSportByChampionship = (championship) ->
      $scope.sport_of_championship[championship.id] = filterFilter($scope.sports, { id: championship.sport_id }, true)[0]
      return

    $scope.reloadCountryByChampionship = (championship) ->
      $scope.country_of_championship[championship.id] = filterFilter($scope.countries, { id: championship.country_id }, true)[0]
      return

    $scope.reloadChampionshipByEvent = (event) ->
      $scope.championship_of_event[event.id] = filterFilter($scope.championships, { id: event.championship_id }, true)[0]
      return

    $scope.reloadNextEvents = ->
      if !$scope.mainDataIsLoaded()
        return false
      sort_next_events = []
      $scope.events.map (event) ->
        if new Date(event.date_event) > new Date
          sort_next_events.push event
        return
      sort_next_events.sort (a, b) ->
        a = new Date(a.date_event)
        b = new Date(b.date_event)
        a - b
      $scope.next_events = []
      sort_next_events.map (event, key) ->
        if key <= 4
          $scope.next_events.push event
          $scope.reloadChampionshipByEvent event
          $scope.reloadSportByChampionship $scope.championship_of_event[event.id]
          $scope.reloadCountryByChampionship $scope.championship_of_event[event.id]
        else
          return false
        return
      return

    $scope.reloadSports()
    $scope.reloadCountries()
    $scope.reloadChampionships()
    $scope.reloadEvents()
    $scope.reloadCoefficients()
    $scope.reloadNextEvents()

    ### on ###

    $scope.$on 'reloadCountries', ->
      $scope.reloadCountries()
      $scope.reloadNextEvents()
      return
    $scope.$on 'reloadSports', ->
      $scope.reloadSports()
      $scope.reloadNextEvents()
      return
    $scope.$on 'reloadChampionships', ->
      $scope.reloadChampionships()
      $scope.reloadNextEvents()
      return
    $scope.$on 'reloadEvents', ->
      $scope.reloadEvents()
      $scope.reloadNextEvents()
      return
    $scope.$on 'reloadCoefficients', ->
      $scope.reloadCoefficients()
      $scope.reloadNextEvents()
      return
    return
]