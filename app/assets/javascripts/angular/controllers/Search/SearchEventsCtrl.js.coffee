angular.module('betlog.search').controller('SearchEvents', [
  '$scope',
  '$http',
  'filterFilter',
  '$animate',
  'Storage'
  ($scope, $http, filterFilter, $animate, Storage) ->
    $scope.str_for_search_teams = ''
    $scope.search_result = []

    $scope.getEvents = ->
      Storage.getEvents()

    $scope.getChampionships = ->
      Storage.getChampionships()

    $scope.getCountries = ->
      Storage.getCountries()

    $scope.getSports = ->
      Storage.getSports()

    $scope.searchEvents = ->
      $scope.search_result = []
      if $scope.str_for_search_teams.length > 2
        searched_events_by_first_team = filterFilter($scope.getEvents(), opponent_1: $scope.str_for_search_teams)
        searched_events_by_second_team = filterFilter($scope.getEvents(), opponent_2: $scope.str_for_search_teams)
        $scope.search_result = searched_events_by_first_team.concat(searched_events_by_second_team)
      return

    $scope.getSearchedChampionshipsResults = ->
      result = []
      championships_ids = []
      if $scope.getSearchedResults().length
        $scope.getSearchedResults().map (event) ->
          championship = filterFilter($scope.getChampionships(), { id: event.championship_id }, true)[0]
          if result.indexOf(championship) == -1 and championship
            result.push championship
          return
      result

    $scope.getSearchedResults = ->
      $scope.search_result

    $scope.getSearchedResultsByChampionship = (championship) ->
      filterFilter $scope.getSearchedResults(), { championship_id: championship.id }, true

    $scope.closeSearch = ->
      $scope.search_result = []
      return

    $scope.getSportByCountry = (country) ->
      if country
        return filterFilter($scope.getSports(), { id: country.sport_id }, true)[0]
      return

    $scope.getCountryByEvent = (event) ->
      filterFilter($scope.getCountries(), { id: event.country_id }, true)[0]

    $scope.getSportByChampionship = (championship) ->
      filterFilter($scope.getSports(), { id: championship.sport_id }, true)[0]

    $scope.getCountryByChampionship = (championship) ->
      filterFilter($scope.getCountries(), { id: championship.country_id }, true)[0]

    $scope.getChampionshipByEvent = (event) ->
      filterFilter($scope.getChampionships(), { id: event.championship_id }, true)[0]

    ###  on  ###

    $scope.$on 'reloadEvents', ->
      $scope.getEvents()
      return
    $scope.$on 'reloadCountries', ->
      Storage.getCountries()
      return
    $scope.$on 'reloadSports', ->
      Storage.getSports()
      return
    $scope.$on 'reloadChampionships', ->
      Storage.getChampionships()
      return
    return
])