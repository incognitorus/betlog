angular.module('betlog.championships').controller 'Championships', [
  '$scope'
  'filterFilter'
  '$stateParams'
  'Storage'
  'active_sport'
  'active_country'
  'active_championship'
  ($scope, filterFilter, $stateParams, Storage, active_sport, active_country, active_championship) ->
    $scope.active_sport = active_sport
    $scope.active_country = active_country
    $scope.active_championship = if active_championship then active_championship else false

    $scope.getAllChampionships = () ->
      return Storage.getChampionships()

    $scope.getSports = () ->
      return Storage.getSports()

    $scope.getCountries = () ->
      return Storage.getCountries()

    $scope.getEvents = () ->
      return Storage.getEvents()

    $scope.active = (championship) ->
      $scope.getChampionships().map (championship, i) ->
        championship.isActive = false
        return
      championship.isActive = true
      return

    $scope.select = (championship) ->
      $scope.activeChampionshipAndSendHimToBroadcast championship
      return

    $scope.getChampionships = (country_id, sport_id) ->
      filterFilter $scope.getAllChampionships(), {
        country_id: parseInt($stateParams.country_id)
        sport_id: parseInt($stateParams.sport_id)
      }, true


    $scope.getChampionshipsBySportAndCountry = ->
      filterFilter $scope.getChampionships(), {
        sport_id: parseInt($stateParams.sport_id)
        country_id: parseInt($stateParams.country_id)
      }, true


    $scope.getChampionshipsByActiveCountryOrFavorites = ->
      filterFilter $scope.getAllChampionships(), {
        country_id: $scope.active_country.id
        sport_id: $scope.active_sport.id
      }, true

    $scope.activeChampionshipAndSendHimToBroadcast = (championship) ->
      $scope.active championship
      $scope.$broadcast 'championshipSelected', championship
      return


    $scope.getEventsAllChampionships = ->
      result = []
      $scope.getChampionshipsBySportAndCountry().map (championship) ->
        championship_events = filterFilter($scope.getEvents(), { championship_id: championship.id }, true)
        championship_events.map (events) ->
          result.push events
          return
        return
      result


    $scope.getEventsByChampionship = (championship) ->
      filterFilter $scope.getEvents(), { championship_id: championship.id }, true

    $scope.getDatesFromCmapionshipEvents = (championship)->
      result = []
      event_scope = if championship then $scope.getEventsByChampionship(championship) else $scope.getEventsAllChampionships()
      event_scope.map (event, i) ->
        day = event.date_event.slice(0, 10)
        if !filterFilter(result, day).length
          result.push day
        return
      result.sort()

    $scope.getCountryName = ->
      if $scope.active_country.length then $scope.active_country[0].name else 'Избранное'


    $scope.getEventsChampionshipByEventDate = (date_event, championship) ->
      result = []
      event_scope = $scope.getEventsByChampionship(championship)
      event_scope.map (event, i) ->
        if event.date_event.indexOf(date_event) != -1
          result.push event
        return
      result


    $scope.getActiveSport = ->
      filterFilter($scope.getSports(), { id: parseInt($stateParams.sport_id) }, true)[0]

    $scope.getActiveCountry = ->
      filterFilter($scope.getCountries(), { id: parseInt($stateParams.country_id) }, true)[0]

    $scope.getActiveChampionship = ->
      filterFilter($scope.getChampionships(), { id: if parseInt($stateParams.championship_id) then parseInt($stateParams.championship_id) else false }, true)[0]


    ### on ###

    $scope.$on('reloadChampionships', () ->
      $scope.getAllChampionships()
    )

    $scope.$on('reloadSports', () ->
      $scope.getSports()
    )

    $scope.$on('reloadCountries', () ->
      $scope.getCountries()
    )

    $scope.$on('reloadEvents', () ->
      $scope.getEvents()
    )


    return
]