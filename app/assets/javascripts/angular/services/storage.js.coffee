angular.module('betlog.services').factory 'Storage', [
  'filterFilter'
  '$http'
  '$rootScope'
  '$cookieStore'
  (filterFilter, $http, $rootScope, $cookieStore) ->
    bookmakers = undefined
    championships = undefined
    coefficients = undefined
    countries = undefined
    current_user = undefined
    events = undefined
    managed_leagues = undefined
    sports = undefined
    sports = []
    countries = []
    championships = []
    events = []
    current_user = {}
    coefficients = []
    managed_leagues = []
    bookmakers = []

    Storage.initSports = ->
      $http.get('/sports.json').success((data, status, headers, config) ->
        if status == 200 and data.length
          data.map (sport, i) ->
            new_sport = undefined
            new_sport =
              id: sport.id
              name: sport.name
              isActive: false
            sports.push new_sport
            return
          $rootScope.$broadcast 'reloadSports'
        return
      ).error (data, status, headers, config) ->
      return

    Storage.initCountries = ->
      $http.get('/countries.json').success((data, status, headers, config) ->
        count_countries_by_sport_id = undefined
        if status == 200 and data.length
          count_countries_by_sport_id = {}
          data.map (country, i) ->
            new_country = undefined
            new_country =
              id: country.id
              name: country.name
              sport_id: country.sport_id
              isActive: false
            countries.push new_country
            return
          $rootScope.$broadcast 'reloadCountries'
        return
      ).error (data, status, headers, config) ->
      return

    Storage.initChampionships = ->
      $http.get('/championships.json').success((data, status, headers, config) ->
        if status == 200 and data.length
          data.map (championship, i) ->
            new_championship = undefined
            new_championship =
              id: championship.id
              name: championship.name
              country_id: championship.country_id
              sport_id: championship.sport_id
              isActive: false
            championships.push new_championship
            return
          $rootScope.$broadcast 'reloadChampionships'
          Storage.initManagedLeagues()
        return
      ).error (data, status, headers, config) ->
      return

    Storage.initEvents = ->
      $http.get('/events.json').success((data, status, headers, config) ->
        if status == 200 and data.length
          data.map (event, i) ->
            new_event = undefined
            new_event =
              id: event.id
              championship_id: event.championship_id
              opponent_1: event.opponent_1
              opponent_2: event.opponent_2
              date_event: event.date_event
              isOpened: false
            events.push new_event
            return
          $rootScope.$broadcast 'reloadEvents'
        return
      ).error (data, status, headers, config) ->
      return

    Storage.initCurrentUser = ->
      $http.get('/user_sessions/current_user').success((data, status, headers, config) ->
        user = undefined
        if status == 200
          user =
            login: data.login
            name: data.name
          Storage.setCurrentUser user
          $rootScope.$broadcast 'reloadCurrentUser'
        return
      ).error (data, status, headers, config) ->
      return

    Storage.initCoefficients = ->
      $http.get('/coefficients.json').success((data, status, headers, config) ->
        if status == 200 and data.coefficients.length
          data.coefficients.map (coefficient, i) ->
            new_coefficients = undefined
            new_coefficients =
              id: coefficient.item.id
              event_id: coefficient.item.event_id
              bookmaker_id: coefficient.item.bookmaker_id
              bookmaker_name: coefficient.item.bookmaker_name
              first: coefficient.item.first
              draw: coefficient.item.draw
              second: coefficient.item.second
              first_or_draw: coefficient.item.first_or_draw
              first_or_second: coefficient.item.first_or_second
              draw_or_second: coefficient.item.draw_or_second
              first_fora: coefficient.item.first_fora
              second_fora: coefficient.item.second_fora
              coeff_first_fora: coefficient.item.coeff_first_fora
              coeff_second_fora: coefficient.item.coeff_second_fora
              total_less: coefficient.item.total_less
              total_more: coefficient.item.total_more
              coeff_first_total: coefficient.item.coeff_first_total
              coeff_second_total: coefficient.item.coeff_second_total
              created_at: coefficient.item.created_at
            coefficients.push new_coefficients
            return
          $rootScope.$broadcast 'reloadCoefficients'
        return
      ).error (data, status, headers, config) ->
      return

    Storage.initBookmakers = ->
      $http.get('/bookmakers.json').success((data, status, headers, config) ->
        if status == 200 and data.length
          data.map (bookmaker, i) ->
            new_bookmaker = undefined
            new_bookmaker =
              id: bookmaker.id
              name: bookmaker.name
              rating: bookmaker.rating
            bookmakers.push new_bookmaker
            return
          $rootScope.$broadcast 'reloadBookmakers'
        return
      ).error (data, status, headers, config) ->
      return

    Storage.getSports = (sport_id) ->
      if sport_id
        return filterFilter(sports,
          id: parseInt(sport_id)
        , true)
      else
        return sports

    Storage.getCountries = (country_id) ->
      if country_id
        return filterFilter(countries,
          id: parseInt(country_id)
        , true)
      else
        return countries

    Storage.getChampionships = (championship_id) ->
      if championship_id
        return filterFilter(championships,
          id: parseInt(championship_id)
        , true)
      else
        return championships

    Storage.getEvents = ->
      events

    Storage.getCoefficients = ->
      coefficients

    Storage.getCurrentUser = ->
      current_user

    Storage.getManagedLeagues = ->
      managed_leagues

    Storage.getBookmakers = ->
      bookmakers

    Storage.setCurrentUser = (data) ->
      current_user = data
      $rootScope.$broadcast 'reloadCurrentUser'
      return

    Storage.hasCurrentUser = ->
      if current_user.login and current_user.name
        true
      else
        false

    Storage.signIn = (data, success_callback) ->
      $http(
        method: 'POST'
        url: '/user_sessions'
        data: data).success((data, status, headers, config) ->
        user = undefined
        if status == 200
          user =
            login: data.login
            name: data.name
          Storage.setCurrentUser user
          $rootScope.$broadcast 'authorization_success'
        return
      ).error (data, status, headers, config) ->
        $rootScope.$broadcast 'authorization_error', data
        return
      return

    Storage.signOut = ->
      $http['delete']('/user_sessions').success((data, status, headers, config) ->
        user = undefined
        if status == 200 and data.status == 'ok'
          user = {}
          Storage.setCurrentUser user
          $rootScope.$broadcast 'signout_success'
        return
      ).error (data, status, headers, config) ->
      return

    Storage.signUp = (data) ->
      $http.post('/users', data).success((data, status, headers, config) ->
        user = undefined
        if status == 200
          user =
            login: data.user.login
            name: data.user.name
          Storage.setCurrentUser user
          $rootScope.$broadcast 'signup_success'
        return
      ).error (data, status, headers, config) ->
      return

    Storage.initManagedLeagues = ->
      championships_ids = undefined
      result = undefined
      result = []
      championships_ids = $cookieStore.get('managedLeagues')
      if championships_ids and championships_ids.length
        championships_ids.map (id) ->
          championship = undefined
          championship = filterFilter(championships, { id: id }, true)[0]
          if championship and result.indexOf(championship) == -1
            result.push championship
          return
        managed_leagues = result
        $rootScope.$broadcast 'reloadManagedLeagues'
      return

    Storage.addChampionshipInToManagerLeagues = (add_championship) ->
      championships_ids = undefined
      championships_ids = $cookieStore.get('managedLeagues')
      if !championships_ids
        championships_ids = []
      if championships_ids.indexOf(add_championship.id) == -1
        championships_ids.push add_championship.id
        managed_leagues.push add_championship
      $rootScope.$broadcast 'reloadManagedLeagues'
      $cookieStore.put 'managedLeagues', championships_ids
      return

    Storage.removeChampionshipFromManagerLeagues = (championship) ->
      championships_ids = undefined
      championships_ids = $cookieStore.get('managedLeagues')
      championships_ids.map (championships_id, key) ->
        if championships_id == championship.id
          championships_ids.splice key, 1
        return
      managed_leagues.map (managed_championship, key) ->
        if managed_championship.id == championship.id
          managed_leagues.splice key, 1
        return
      $rootScope.$broadcast 'reloadManagedLeagues'
      $cookieStore.put 'managedLeagues', championships_ids
      return

    Storage.initSports()
    Storage.initCountries()
    Storage.initChampionships()
    Storage.initEvents()
    Storage.initCoefficients()
    #Storage.initCurrentUser()
    Storage.initBookmakers()
    Storage
]