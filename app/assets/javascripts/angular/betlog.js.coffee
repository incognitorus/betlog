getRandom = (min, max) ->
  Math.floor(Math.random() * (max - min + 1)) + min

angular.module("betlog", [
  "ngAnimate"
  "ngCookies"
  "ui.router"
  "templates"
  "betlog.favorites"
  "betlog.next_events"
  "betlog.manager_leagues"
  "betlog.menu"
  "betlog.search"
  "betlog.services"
  "betlog.directives"
  "betlog.events"
  "betlog.championships"
  "betlog.sports"
]).config(["$stateProvider", "$urlRouterProvider", ($stateProvider, $urlRouterProvider) ->
  $stateProvider
    .state("home",
      url: "/"
      controller: "FavoriteEvents"
      templateUrl: "Favorites/favorites.index.html"
    )
    .state("sports",
      abstract: true
      url: "/sports"
      templateUrl: "Championships/championships.index.html"
    )
    .state("sports.countries_championships",
      url: "/:sport_id/countries/:country_id/championships"
      templateUrl: "Championships/championships.list.html"
      controller: "Championships"
      resolve:
        active_sport: ($stateParams) ->
          return Storage.getSports($stateParams.sport_id)[0]
        active_country: ($stateParams) ->
          return Storage.getCountries($stateParams.country_id)[0]
        active_championship: ($stateParams) ->
          return false
    )
    .state("championships",
      abstract: true
      url: "/sports/:sport_id/countries/:country_id/championships"
      templateUrl: "Championships/championships.index.html"
    )
    .state("championships.detail",
      url: "/:championship_id"
      templateUrl: "Championships/championships.detail.html"
      controller: "Championships"
      resolve:
        active_sport: ($stateParams) ->
          return Storage.getSports($stateParams.sport_id)[0]
        active_country: ($stateParams) ->
          return Storage.getCountries($stateParams.country_id)[0]
        active_championship: ($stateParams) ->
          return Storage.getChampionships($stateParams.championship_id)[0]
    )
  ]).config([ "$httpProvider", ($httpProvider) ->
    $httpProvider.defaults.headers.common["X-Requested-With"] = "XMLHttpRequest"
  ]).config(["$locationProvider", ($locationProvider) ->
    $locationProvider.html5Mode(true);
  ]).run(->
    console.log 'Betlog running'
  )
