angular.module('betlog.directives').directive('fancyboxLogin', [
  '$rootScope'
  ($rootScope) ->
    {
      restrict: 'AC'
      link: (scope, element) ->
        $rootScope.$broadcast 'showAuthorization'
        return

    }
]).directive('nameValidate', [
  '$http'
  ($http) ->
    {
      require: 'ngModel'
      link: (scope, elm, attrs, ctrl) ->
        ctrl.$parsers.unshift (viewValue) ->
          scope.nameHasRequired = undefined
          scope.nameValidate = false
          scope.nameHasRequired = if viewValue and viewValue.length > 0 then false else true
          if !scope.nameHasRequired
            scope.nameValidate = true
            viewValue
          else
            undefined
        return

    }
]).directive('loginValidate', [
  '$http'
  ($http) ->
    {
      require: 'ngModel'
      link: (scope, elm, attrs, ctrl) ->
        ctrl.$parsers.unshift (viewValue) ->
          scope.lgHasRequired = undefined
          scope.lgHasEmail = undefined
          scope.lgAlredyExists = undefined
          scope.loginValidate = false
          scope.lgHasRequired = if viewValue and viewValue.length > 0 then false else true
          scope.lgHasEmail = if viewValue and /^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/.test(viewValue) then false else true
          $http(
            method: 'GET'
            url: '/users/user_exists?login=' + viewValue).success((data, status, headers, config) ->
            scope.lgAlredyExists = true
            #change loginvalidete after return response
            scope.loginValidate = false
            return
          ).error (data, status, headers, config) ->
            scope.lgAlredyExists = false
            return
          if !scope.lgHasRequired and !scope.lgHasEmail and !scope.lgAlredyExists
            scope.loginValidate = true
            viewValue
          else
            undefined
        return

    }
]).directive('passwordValidate', [ ->
  {
    require: 'ngModel'
    link: (scope, elm, attrs, ctrl) ->
      ctrl.$parsers.unshift (viewValue) ->
        scope.pwdValidLength = undefined
        scope.pwdHasLetter = undefined
        scope.pwdHasNumber = undefined
        scope.pwdValidate = false
        scope.pwdValidLength = if viewValue and viewValue.length >= 8 then false else true
        scope.pwdHasLetter = if viewValue and /[A-z]/.test(viewValue) then false else true
        if !scope.pwdHasLetter and !scope.pwdHasNumber and !scope.pwdValidLength
          scope.pwdValidate = true
          viewValue
        else
          undefined
      return

  }
 ]).directive('eatClick', [ ->
  (scope, element, attrs) ->
    $(element).on 'click', (event) ->
      event.preventDefault()
      return
    return
 ]).directive('slideable', ->
  {
    restrict: 'C'
    compile: (element, attr) ->
      # wrap tag
      contents = element.html()
      element.html '<div class="slideable_content">' + contents + '</div>'
      (scope, element, attrs) ->
        # default properties
        attrs.duration = if !attrs.duration then '0.3s' else attrs.duration
        attrs.easing = if !attrs.easing then 'ease-in-out' else attrs.easing
        element.css
          'overflow': 'hidden'
          'height': '0px'
          'transitionProperty': 'height'
          'transitionDuration': attrs.duration
          'transitionTimingFunction': attrs.easing
        return

  }
).directive('slideToggle', ->
  {
    restrict: 'A'
    link: (scope, element, attrs) ->
      target = element.next().get(0)
      attrs.expanded = false
      element.bind 'click', ->
        content = target.querySelector('.slideable_content')
        if element.hasClass('hidden-leagues')
          element.removeClass 'hidden-leagues'
          element.addClass 'visibled-leagues'
        else
          element.removeClass 'visibled-leagues'
          element.addClass 'hidden-leagues'
        if !attrs.expanded
          content.style.border = '1px solid rgba(0,0,0,0)'
          y = content.clientHeight
          content.style.border = 0
          target.style.height = y + 'px'
        else
          target.style.height = '0px'
        attrs.expanded = !attrs.expanded
        return
      return

  }
).directive('tabs', ->
  {
    restrict: 'A'
    link: (scope, element, attrs) ->
      tab_titles = element.find('div.tab-title-item')
      tab_titles.on 'click', ->
        element.find('div.tab-content').removeClass 'active'
        element.find('div.tab-title-item').removeClass 'active'
        $(this).addClass 'active'
        index = element.find('div.tab-title-item').index($(this))
        jQuery(element.find('div.tab-content')[index]).addClass 'active'
        return
      tab_titles[0].click()
      return

  }
).directive('eventGraph', ->
  {
    restrict: 'A'
    link: (scope, element, attrs) ->
      element.parent().on 'mouseover', ->
        if !element.find('div.highcharts-container').length
          element.highcharts
            title:
              text: 'История изменения коэффициента'
              x: -20
            xAxis: categories: scope.$eval(attrs.dates)
            yAxis:
              title: text: ''
              plotLines: [ {
                value: 0
                width: 0.5
                color: '#808080'
              } ]
            tooltip: valueSuffix: ''
            legend: enabled: false
            series: [ {
              name: attrs.paramName
              data: scope.$eval(attrs.coefficients)
            } ]
            exporting: enabled: false
        return
      return

  }
).directive 'jcarousel', ->
  {
    restrict: 'C'
    compile: (element, attr) ->
      (scope, element, attrs) ->
        (($) ->
          $ ->
            $('.jcarousel').jcarousel wrap: 'circular'
            $('.hot-matches-left').on('jcarouselcontrol:active', ->
              $(this).removeClass 'inactive'
              return
            ).on('jcarouselcontrol:inactive', ->
              $(this).addClass 'inactive'
              return
            ).jcarouselControl target: '-=1'
            $('.hot-matches-right').on('jcarouselcontrol:active', ->
              $(this).removeClass 'inactive'
              return
            ).on('jcarouselcontrol:inactive', ->
              $(this).addClass 'inactive'
              return
            ).jcarouselControl target: '+=1'
            return
          return
        ) jQuery
        return

  }