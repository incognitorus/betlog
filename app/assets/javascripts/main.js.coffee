$(document).ready ->
  
  # Search block
  $("div.search input").placeholder()
  
  $("div.search input").on "focus", ->
    $(this).attr "placeholder", ""
    $("div.search").removeClass "hide"
    $(this).animate
      width: "250px"
    , 200
    return

  $("div.search input").on "blur", ->
    $(this).attr "placeholder", "Введите команду..."
    $("div.search").addClass "hide"
    $(this).animate
      width: "130px"
    , 200
    return

  
  # event 
  $("div.event div.event-title").on "click", ->
    if $(this).next().is(":visible")
      $(this).next().slideUp()
      $(this).closest("div.event").find("div.event-hide-full").removeClass("event-hide-full").addClass "event-show-full"
    else
      $(this).next().slideDown()
      $(this).closest("div.event").find("div.event-show-full").removeClass("event-show-full").addClass "event-hide-full"
    return

  return
