ActiveAdmin.register Championship do
  menu parent: 'Чемпионат'
  permit_params :id,
    :title,
    :sport_id,
    championship_associations_attributes: [:id, :title, :championship_id, :_destroy],
    teams_attributes: [:id, :title, :championship_id, :_destroy]


  filter :title
  filter :sport
  filter :created_at
  filter :updated_at

  index do
    selectable_column
    column :title
    column :sport
    column :created_at
    column :updated_at
    actions
  end

  show do |sport|
    attributes_table do
      row :id
      row :title
      row :sport

      panel 'Ассоциации чемпионата' do
        table_for championship.championship_associations do
          
          column 'запись' do |championship_association|
            championship_association.title
          end
        end
      end

      panel 'Участники чемпионата' do
        table_for championship.teams do
          
          column 'запись' do |team|
            link_to team.title, admin_team_path(team)
          end
        end
      end
    end
  end

  form do |f|
    f.semantic_errors *f.object.errors.keys

    f.inputs 'Основные данные' do
      f.input :title
      f.input :sport
    end

    f.inputs 'Ассоциации чемпионата' do
      f.has_many :championship_associations, allow_destroy: true, new_record: true do |ca|
        ca.input :title
      end
    end

    f.inputs 'Участники чемпионата' do
      f.has_many :teams, allow_destroy: true, new_record: true do |t|
        t.input :title
      end
    end

    f.actions
  end

  controller do
    def create
      super do |success,failure|
        success.html { redirect_to admin_championships_path }
      end
    end

    def update
      super do |success,failure|
        success.html { redirect_to admin_championships_path }
      end
    end
  end
end