ActiveAdmin.register SportAssociation do
  menu parent: 'Спорт ассоциации'
  permit_params :id,
    :title,
    :sport_id

  filter :title
  filter :sport
  filter :created_at
  filter :updated_at

  index do
    selectable_column
    column :title
    column :sport
    column :created_at
    column :updated_at
    actions
  end

  show do |sport_association|
    attributes_table do
      row :id
      row :title
      row :sport
    end
  end

  form do |f|
    f.semantic_errors *f.object.errors.keys

    f.inputs 'Основные данные' do
      f.input :title
      f.input :sport
    end

    f.actions
  end

  controller do
    def create
      super do |success,failure|
        success.html { redirect_to admin_sport_associations_path }
      end
    end

    def update
      super do |success,failure|
        success.html { redirect_to admin_sport_associations_path }
      end
    end
  end
end