ActiveAdmin.register Team do
  menu parent: 'Команда'
  permit_params :id,
    :title,
    :championship_id,
    team_associations_attributes: [:id, :title, :team_id, :_destroy]

  filter :title
  filter :championship
  filter :created_at
  filter :updated_at

  index do
    selectable_column
    column :title
    column :championship
    column :created_at
    column :updated_at
    actions
  end

  show do |team|
    attributes_table do
      row :id
      row :title
      row :championship
    end
  end

  form do |f|
    f.semantic_errors *f.object.errors.keys

    f.inputs 'Основные данные' do
      f.input :title
      f.input :championship_id, :as => :string
    end

    f.inputs 'Ассоциации' do
      f.has_many :team_associations, allow_destroy: true, new_record: true do |sa|
        sa.input :title
      end
    end

    f.actions
  end

  controller do
    def create
      super do |success,failure|
        success.html { redirect_to admin_teams_path }
      end
    end

    def update
      super do |success,failure|
        success.html { redirect_to admin_teams_path }
      end
    end
  end
end