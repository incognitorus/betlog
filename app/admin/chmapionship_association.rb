ActiveAdmin.register ChampionshipAssociation do
  menu parent: 'Чемпионат ассоциации'
  permit_params :id,
    :title,
    :championship_id

  filter :title
  filter :championship
  filter :created_at
  filter :updated_at

  index do
    selectable_column
    column :title
    column :championship
    column :created_at
    column :updated_at
    actions
  end

  show do |sport_association|
    attributes_table do
      row :id
      row :title
      row :championship
    end
  end

  form do |f|
    f.semantic_errors *f.object.errors.keys

    f.inputs 'Основные данные' do
      f.input :title
      f.input :championship
    end

    f.actions
  end

  controller do
    def create
      super do |success,failure|
        success.html { redirect_to admin_championship_associations_path }
      end
    end

    def update
      super do |success,failure|
        success.html { redirect_to admin_championship_associations_path }
      end
    end
  end
end