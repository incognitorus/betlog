ActiveAdmin.register Sport do
  menu parent: 'Спорт'
  permit_params :id,
    :title,
    sport_associations_attributes: [:id, :title, :sport_id, :_destroy],
    chmapionships_attributes: [:id, :title, :sport_id, :_destroy]

  filter :title
  filter :created_at
  filter :updated_at

  index do
    selectable_column
    column :title
    column :created_at
    column :updated_at
    actions
  end

  show do |sport|
    attributes_table do
      row :id
      row :title

      panel 'Чемпионат' do
        table_for sport.championships do
          
          column 'запись' do |championship|
            link_to championship.title, edit_admin_championship_path(championship)
          end
        end
      end

      panel 'Ассоциации' do
        table_for sport.sport_associations do
          
          column 'запись' do |sport_association|
            sport_association.title
          end
        end
      end
    end
  end

  form do |f|
    f.semantic_errors *f.object.errors.keys

    f.inputs 'Основные данные' do
      f.input :title
    end

    f.inputs 'Чемпионаты' do
      f.has_many :championships, allow_destroy: true, new_record: true do |ch|
        ch.input :title
      end
    end

    f.inputs 'Ассоциации' do
      f.has_many :sport_associations, allow_destroy: true, new_record: true do |sa|
        sa.input :title
      end
    end

    f.actions
  end

  controller do
    def create
      super do |success,failure|
        success.html { redirect_to admin_sports_path }
      end
    end

    def update
      super do |success,failure|
        success.html { redirect_to admin_sports_path }
      end
    end
  end
end