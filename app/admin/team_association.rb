ActiveAdmin.register TeamAssociation do
  menu parent: 'Команда ассоциация'
  permit_params :id,
    :title,
    :team_id

  filter :title
  filter :team
  filter :created_at
  filter :updated_at

  index do
    selectable_column
    column :title
    column :team
    column :created_at
    column :updated_at
    actions
  end

  show do |team_association|
    attributes_table do
      row :id
      row :title
      row :team
    end
  end

  form do |f|
    f.semantic_errors *f.object.errors.keys

    f.inputs 'Основные данные' do
      f.input :title
      f.input :team
    end

    f.actions
  end

  controller do
    def create
      super do |success,failure|
        success.html { redirect_to admin_team_associations_path }
      end
    end

    def update
      super do |success,failure|
        success.html { redirect_to admin_team_associations_path }
      end
    end
  end
end