module Parser
  class Parse
    def initialize bookmaker
      @parse_class = bookmaker
    end

    def run
      bookmaker = @parse_class.new
      bookmaker.run
    end
  end
end