namespace :parser do
  desc "Parser go williamhill"
  task :williamhill => :environment do
    require 'parser'
    require 'parser/williamhill'

    bookmaker = Parser::WilliamHill
    parser = Parser::Parse.new bookmaker
    parser.run
  end

  desc "Parser go parimatch"
  task :parimatch => :environment do
    require 'parser'
    require 'parser/parimatch'

    bookmaker = Parser::PariMatch
    parser = Parser::Parse.new bookmaker
    parser.run
  end

  desc "Parser go bwin"
  task :bwin => :environment do
    require 'parser'
    require 'parser/bwin'

    bookmaker = Parser::Bwin
    parser = Parser::Parse.new bookmaker
    parser.run
  end
end

