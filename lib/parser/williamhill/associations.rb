module Parser
  class WilliamHill
    class Associations
      def self.get title, category
        associations = {
          'Football' => { category: 'FOOTBALL', associations: ['UEFA Club Competitions', 'International Football', 'UK Football', 'European Major Leagues', 'Other League Football']},
          'Basketball' => { category: 'BASKETBALL', associations: ['US Basketball', 'European Basketball']},
          'American football' => { category: 'AMERICAN_FB', associations: ['American Football']},
          'Baseball' => { category: 'BASEBALL', associations: ['Baseball']},
          'Golf' => { category: 'GOLF', associations: ['Mens Golf', 'Golf Majors', 'Womens Golf']},
          'Cricket' => { category: 'CRICKET', associations: ['International Cricket', 'UK Domestic Cricket']},
          'Darts' => { category: 'DARTS', associations: ['Darts']},
          'Hockey' => { category: 'ICE_HOCKEY', associations: ['International Ice Hockey', 'NHL', 'European Ice Hockey']},
          'Motor racing' => { category: 'MOTOR_RACING', associations: ['Formula 1', 'Motor', 'Speedway']},
          'Motor bikes' => { category: 'MOTORBIKES', associations: ['Moto GP', 'Speedway', 'Superbikes']},
          'Rugby League' => { category: 'RUGBY_LEAGUE', associations: ['Rugby League']},
          'Rugby Union' => { category: 'RUGBY_UNION', associations: ['Rugby Union']},
          'Snooker' => { category: 'SNOOKER', associations: ['Snooker']},
          'Tennis' => { category: 'TENNIS', associations: ['Grand Slam Tennis', 'Mens Tennis', 'Womens Tennis', 'Challenger Tennis']},
          'Badminton' => { category: 'OTHER_SPORTS', associations: ['Badminton']},
          'Beach Volleyball' => { category: 'OTHER_SPORTS', associations: ['Beach Volleyball']},
          'Boxing' => { category: 'OTHER_SPORTS', associations: ['Boxing']},
          'Cycling' => { category: 'OTHER_SPORTS', associations: ['Cycling']},
          'Handball' => { category: 'OTHER_SPORTS', associations: ['Handball']},
          'Volleyball' => { category: 'OTHER_SPORTS', associations: ['Volleyball']}
        }

        associations.each do |sport_key, sport|
          if sport[:category] == category
            sport[:associations].each do |association|
              if association == title
                return sport_key
              end
            end
          end
        end

        return false
      end
    end
  end
end