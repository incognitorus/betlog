module Parser
  require 'open-uri'
  require 'nokogiri'
  require 'httpclient'

  class Bwin
    #require 'parser/bwin/associations'

    def run
      categories  = get_categories

      bookmaker = Bookmaker.find_by_title('Bwin')
      
      Nokogiri::HTML(get_sport_page.body).css('sport').each do |sport_tag|
        if categories.include? sport_tag["name"]
          
          sport_page_association_name = sport_tag["name"]

          if SportAssociation.find_by_title(sport_page_association_name).present?
            sport = SportAssociation.find_by_title(sport_page_association_name).sport

            sport_tag.css("league").each do |championship_tag|

              championship_page_name = "#{championship_tag['regionname']} #{championship_tag['name']}"

              if ChampionshipAssociation.find_by_title(championship_page_name).present?
                championship = get_championship_by_title_and_sport_id championship_page_name, sport.id
                
                if championship.present?
                  puts "--- ok #{championship.title} ---" 

                  date_block = Nokogiri::HTML(get_championship_page(championship_tag['groupid'], sport_tag['id']).body)
                    .css('#markets > .ui-widget-content > .ui-widget-content-body')
                  
                  if date_block.size > 1
                    date_event_without_time = date_block.css('h2.event-group-level1')[0].text.split(" - ")[1].strip

                    date_block.css('ul li ul li ul li ul li').each do |event_tag|

                      if event_tag.css('tr.col3 td.option').size == 3

                        team1_page_name = event_tag.css('tr.col3 td.option')[0].css('span.option-name').text.strip
                        team2_page_name = event_tag.css('tr.col3 td.option')[2].css('span.option-name').text.strip

                        team1_association = TeamAssociation.find_by_title team1_page_name
                        team2_association = TeamAssociation.find_by_title team2_page_name

                        unless team1_association
                          team_association_not_found team1_page_name, championship.id, sport.title

                          open('bwin.txt', 'a') { |f|
                            f.puts "--- team association #{team1_page_name} now found ---"
                            f.puts "--- championship id #{championship.id} not found ---"
                            f.puts "--- sport: #{sport.title} ---"
                          }

                          unless team2_association
                            team_association_not_found team2_page_name, championship.id, sport.title

                            open('bwin.txt', 'a') { |f|
                              f.puts "--- team association #{team2_page_name} now found ---"
                              f.puts "--- championship id #{championship.id} not found ---"
                              f.puts "--- sport: #{sport.title} ---"
                            }
                          end 

                          if team1_association and team2_association

                            puts "--- ok #{team1_association.title} ---"
                            puts "--- ok #{team2_association.title} ---"
                          
                            event_obj = {
                              opponent_1: team1_association.team.id,
                              opponent_2: team2_association.team.id,
                              date_event: DateTime.parse("#{event_tag['date']} #{event_tag['time']}")
                            }
                        
                            unless event_type_one_already_exists event_obj
                              event = Event.new({ championship_id: championship.id })

                              if event.save
                                event_obj[:event_id] = event.id

                                event_type_one = EventTypeOne.create event_obj
                              end

                              puts "new item"
                            else
                              event_type_one = get_event_type_one_by_data event_obj
                              puts "already exist"
                            end

                            event_tag.css("participant").each do |coefficient|
                              puts "--- ok coeffs ---"
                              if coefficient["name"] == team1_page_name
                                puts "--- ok coeffs for #{team1_page_name} ---"
                                Coefficient.create({
                                  title: '1',
                                  value: coefficient["oddsdecimal"],
                                  event_type_one_id: event_type_one.id,
                                  bookmaker_id: bookmaker.id
                                })
                              elsif coefficient["name"] == team2_page_name
                                puts "--- ok coeffs for #{team2_page_name} ---"
                                Coefficient.create({
                                  title: '2',
                                  value: coefficient["oddsdecimal"],
                                  event_type_one_id: event_type_one.id,
                                  bookmaker_id: bookmaker.id
                                })
                              elsif coefficient["name"] == "Draw"
                                puts "--- ok coeffs for draw---"
                                Coefficient.create({
                                  title: 'x',
                                  value: coefficient["oddsdecimal"],
                                  event_type_one_id: event_type_one.id,
                                  bookmaker_id: bookmaker.id
                                })
                              end
                            end
                          end
                        end
                      end
                    end
                  end
                else
                  championship_association_not_found championship_page_name, sport_page_association_name
                end
              else
                championship_association_not_found championship_page_name, sport_page_association_name
              end
            end
          else
            sport_association_not_found sport_page_association_name
          end
        end
      end
    end

    def get_domain
      "http://0s.oj2tc.ovxgsytfoqxgg33n.cmle.ru/betting"
    end

    def event_type_one_already_exists event_obj
      return true if get_event_type_one_by_data event_obj
    end

    def get_event_type_one_by_data event_obj
      return EventTypeOne.find_by_opponent_1_and_opponent_2(event_obj[:opponent_1], event_obj[:opponent_2])
    end

    def get_championship_by_title_and_sport_id title, sport_id
      ChampionshipAssociation.where(title: title).each do |championship_association|
        if championship_association.championship.sport_id == sport_id
          return championship_association.championship
        end
      end
      nil
    end

    def get_championship_page championship_id, sport_id
      http = HTTPClient.new
      body = { 'sportId' => sport_id, 'leagueIds' => championship_id }
      http.post "http://0s.onyg64tuom.mj3ws3romnxw2.cmle.ru/en/sports/indexmultileague", body
    end

    def get_sport_page
      http = HTTPClient.new
      http.transparent_gzip_decompression = true
      http.get "http://0s.mjsxi5djnztq.mfygs.mj3ws3romnxw2.cmle.ru/V2/CalendarFeed.svc/?x-bwin-accessId=MDVhZDk4MjgtYzQzZC00ZjU3LTlkNzktMGI1ZjgzNjNjNDYx"
    end

    def get_categories
      [
        'Football',
        'Tennis',
        'Ice Hockey',
        'Basketball',
        'Volleyball',
        'Handball',
        'Rugby Union'
      ]
    end

    def sport_association_not_found sport_name
      puts "--- sport association #{sport_name} not found ---"
    end

    def championship_association_not_found championship_name, sport_name
      puts "--- championship association #{championship_name} not found ---"
      puts "--- sport: #{sport_name} ---"
    end

    def team_association_not_found team, championship_name, sport_name
      puts "--- team association #{team} now found ---"
      puts "--- championship id #{championship_name} not found ---"
      puts "--- sport: #{sport_name} ---"
    end
  end
end