module Parser
  require 'open-uri'
  require 'nokogiri'
  require 'httpclient'

  class PariMatch
    require 'parser/williamhill/associations'

    def run
      http = HTTPClient.new
      #http.transparent_gzip_decompression = true

      bookmaker = Bookmaker.find_by_title('PariMatch')


      get_link.each do |sport|
        sport_page = http.get(sport,:follow_redirect => true)

        xml_sport = Nokogiri::HTML(sport_page.body)

        sport_page_association_name = xml_sport.css('.container h3').first.text
        
        if SportAssociation.find_by_title(sport_page_association_name).present?
          sport = SportAssociation.find_by_title(sport_page_association_name).sport

          xml_sport.css('.container #sports li a').each do |championship_link|
            championship_page_association_name = championship_link.text
            
            if ChampionshipAssociation.find_by_title(championship_page_association_name).present?
              championship = get_championship_by_title_and_sport_id championship_page_association_name, sport.id

              if championship.present?
              
                championship_page = http.get("#{self.get_domain}#{championship_link['href']}", :follow_redirect => true)

                win_first_index = false;
                draw_index = false
                win_second_index = false;

                Nokogiri::HTML(championship_page.body).css('.wrapper .dt tbody').each_with_index do |event_row, index|
                  if index == 0
                    event_row.parent.css('tr')[0].css('th').each_with_index do |th, index|
                      if th.text.encode.strip == "П1"
                        win_first_index = index;
                      end
                      if th.text.encode.strip == "X"
                        draw_index = index;
                      end
                      if th.text.encode.strip == "П2"
                        win_second_index = index;
                      end
                    end
                  end
                  



                  if win_first_index.present? and draw_index.present? and win_second_index.present?
                    if event_row['class'].include? 'row'
                      unless event_row['class'].include? 'prop'
                        td_tags = event_row.css('tr.bk td')

                        opponent_1 = ''
                        opponent_2 = ''

                        if td_tags[2].css('a').size == 1
                          if td_tags[2].css('a br').size == 1
                            opponent_1 = td_tags[2].css('a').children.to_s.gsub('<img src="./img/stats.gif">', '').split('<br>')[0].encode.strip
                          else
                            td_tags[2].css('a').remove
                            opponent_1 = td_tags[2].children.to_s.split('<br>')[0].encode.strip
                          end
                        elsif td_tags[2].css('a').size == 0
                          opponent_1 = td_tags[2].children.to_s.gsub('<img src="./img/stats.gif">', '').split('<br>')[0].encode.strip
                        elsif td_tags[2].css('a').size == 2
                          opponent_1 = td_tags[2].css('a')[1].children.to_s.split('<br>')[0].encode.strip
                        end

                        if td_tags[2].css('a').size == 1
                          if td_tags[2].css('a br').size == 1
                            opponent_2 = td_tags[2].css('a').children.to_s.gsub('<img src="./img/stats.gif">', '').split('<br>')[1].encode.strip
                          else
                            td_tags[2].css('a').remove
                            opponent_2 = td_tags[2].children.to_s.split('<br>')[1].encode.strip
                          end
                        elsif td_tags[2].css('a').size == 0
                          opponent_2 = td_tags[2].children.to_s.gsub('<img src="./img/stats.gif">', '').split('<br>')[1].encode.strip
                        elsif td_tags[2].css('a').size == 2
                          opponent_2 = td_tags[2].css('a')[1].children.to_s.split('<br>')[1].encode.strip
                        end

                        team1_association = TeamAssociation.find_by_title opponent_1
                        team2_association = TeamAssociation.find_by_title opponent_2

                        unless team1_association
                          team_association_not_found opponent_1, championship.id, sport.title

                          open('parimatch.txt', 'a') { |f|
                            f.puts "--- team association #{opponent_1} now found ---"
                            f.puts "--- championship id #{championship.id} not found ---"
                            f.puts "--- sport: #{sport_page_association_name} ---"
                          }
                        end

                        unless team2_association
                          team_association_not_found opponent_2, championship.id, sport.title

                          open('parimatch.txt', 'a') { |f|
                            f.puts "--- team association #{opponent_2} now found ---"
                            f.puts "--- championship id #{championship.id} not found ---"
                            f.puts "--- sport: #{sport_page_association_name} ---"
                          }
                        end 

                        if team1_association and team2_association
                          event_obj = {
                            opponent_1: team1_association.team.id,
                            opponent_2: team2_association.team.id,
                            date_event: DateTime.strptime("#{td_tags[1].text.slice(0,5)} #{td_tags[1].text.slice(5,10)}", '%d/%m %H:%M')
                          }

                          unless event_type_one_already_exists event_obj
                            event = Event.new({ championship_id: championship.id })

                            if event.save
                              event_obj[:event_id] = event.id

                              event_type_one = EventTypeOne.create event_obj
                            end

                            puts "new item"
                          else
                            event_type_one = get_event_type_one_by_data event_obj
                            puts "already exist"
                          end


                          index_for_first_coeff = get_index_for_coeff td_tags, win_first_index
                          if td_tags[index_for_first_coeff].present?
                            Coefficient.create({
                              title: '1',
                              value: td_tags[index_for_first_coeff].text.encode.strip,
                              event_type_one_id: event_type_one.id,
                              bookmaker_id: bookmaker.id
                            })
                          end

                          index_for_draw_coeff = get_index_for_coeff td_tags, draw_index
                          if td_tags[index_for_draw_coeff].present?
                            Coefficient.create({
                              title: 'x',
                              value: td_tags[index_for_draw_coeff].text.encode.strip,
                              event_type_one_id: event_type_one.id,
                              bookmaker_id: bookmaker.id
                            })
                          end

                          index_for_second_coeff = get_index_for_coeff td_tags, win_second_index
                          if td_tags[index_for_second_coeff].present?
                            Coefficient.create({
                              title: '2',
                              value: td_tags[index_for_second_coeff].text.encode.strip,
                              event_type_one_id: event_type_one.id,
                              bookmaker_id: bookmaker.id
                            })
                          end
                        end
                      end
                    end
                  end
                end
              else
                championship_association_not_found championship_page_association_name, sport_page_association_name
              end
            else
              championship_association_not_found championship_page_association_name, sport_page_association_name
            end
          end
        else
          sport_association_not_found sport_page_association_name
        end
      end
    end

    def get_link
      return [
        "#{self.get_domain}/sport/basketbol",
        #'http://www.parimatch.com.ext.zawq.ru/sport/avto-motosport,
        "#{self.get_domain}/sport/boks",
        "#{self.get_domain}/sport/velosport",
        "#{self.get_domain}/sport/volejjbol",
        "#{self.get_domain}/sport/gandbol",
        "#{self.get_domain}/sport/golf",
        "#{self.get_domain}/sport/tennis",
        "#{self.get_domain}/sport/futbol",
        "#{self.get_domain}/sport/futzal",
        "#{self.get_domain}/sport/khokkejj",
        "#{self.get_domain}/sport/khokkejj-na-trave",
        "#{self.get_domain}/sport/khokkejj-s-mjachom"
      ]
    end

    def get_championship_by_title_and_sport_id title, sport_id
      ChampionshipAssociation.where(title: title).each do |championship_association|
        if championship_association.championship.sport_id == sport_id
          return championship_association.championship
        end
      end
      nil
    end

    def get_domain
      "http://o53xo.obqxe2lnmf2gg2bomnxw2.cmle.ru"
    end

    def event_type_one_already_exists event_obj
      return true if get_event_type_one_by_data event_obj
    end

    def get_event_type_one_by_data event_obj
      return EventTypeOne.find_by_opponent_1_and_opponent_2(event_obj[:opponent_1], event_obj[:opponent_2])
    end

    def get_index_for_coeff td_tags, th_index
      th_with_colspan_count = 0
      td_tags.each_with_index do |td, index|
        if th_with_colspan_count < th_index
          th_with_colspan_count += 1
          
          if td['colspan'].present?
            th_with_colspan_count += td['colspan'].to_i - 1
          end
        else
          return index
        end
      end
    end

    def sport_association_not_found sport_name
      puts "--- sport association #{sport_name} not found ---"
    end

    def championship_association_not_found championship_name, sport_name
      puts "--- championship association #{championship_name} not found ---"
      puts "--- sport: #{sport_name} ---"
    end

    def team_association_not_found team, championship_name, sport_name
      puts "--- team association #{team} now found ---"
      puts "--- championship id #{championship_name} not found ---"
      puts "--- sport: #{sport_name} ---"
    end
  end
end