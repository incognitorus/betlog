module Parser
  require 'open-uri'
  require 'nokogiri'
  require 'httpclient'

  class WilliamHill
    require 'parser/williamhill/associations'

    def run
      categories  = git_categories

      bookmaker = Bookmaker.find_by_title('WilliamHill')
      
      Nokogiri::HTML(get_sport_page.body).css('response > class').each do |sport|
        if categories.include? sport["category"]
          
          sport_page_id = sport["id"]
          sport_page_association_name = Parser::WilliamHill::Associations.get( sport["name"], sport["category"] )

          if sport_page_association_name.present?
            
            if SportAssociation.find_by_title(sport_page_association_name).present?
              sport = SportAssociation.find_by_title(sport_page_association_name).sport

              xml_events = Nokogiri::HTML(get_events_page_by_sport_id(sport_page_id).body)
              xml_events.css("response williamhill class[id='#{sport_page_id}']").each do |sport_tag|
                
                sport_tag.css("type").each do |championship_tag|
                  
                  championship_page_name = championship_tag["name"]

                  if ChampionshipAssociation.find_by_title(championship_page_name).present?
                    championship = get_championship_by_title_and_sport_id championship_page_name, sport.id
                    
                    if championship.present?
                      championship_tag.css("market").each do |event_tag|
                        
                        if event_tag["name"].include? "Match Betting"
                          opponents_str = event_tag["name"].split("-")[0]

                          if opponents_str.split(" v ").size == 2
                            team1_page_name = opponents_str.split(" v ")[0].strip
                            team2_page_name = opponents_str.split(" v ")[1].strip

                            team1_association = TeamAssociation.find_by_title team1_page_name
                            team2_association = TeamAssociation.find_by_title team2_page_name

                            unless team1_association
                              team_association_not_found team1_page_name, championship.id, sport.title

                              open('williamhill.txt', 'a') { |f|
                                f.puts "--- team association #{team1_page_name} now found ---"
                                f.puts "--- championship id #{championship.id} not found ---"
                                f.puts "--- sport: #{sport.title} ---"
                              }
                            end

                            unless team2_association
                              team_association_not_found team2_page_name, championship.id, sport.title

                              open('williamhill.txt', 'a') { |f|
                                f.puts "--- team association #{team2_page_name} now found ---"
                                f.puts "--- championship id #{championship.id} not found ---"
                                f.puts "--- sport: #{sport.title} ---"
                              }
                            end 

                            if team1_association and team2_association
                              event_obj = {
                                opponent_1: team1_association.team.id,
                                opponent_2: team2_association.team.id,
                                date_event: DateTime.parse("#{event_tag['date']} #{event_tag['time']}")
                              }
                          
                              unless event_type_one_already_exists event_obj
                                event = Event.new({ championship_id: championship.id })

                                if event.save
                                  event_obj[:event_id] = event.id

                                  event_type_one = EventTypeOne.create event_obj
                                end

                                puts "new item"
                              else
                                event_type_one = get_event_type_one_by_data event_obj
                                puts "already exist"
                              end

                              event_tag.css("participant").each do |coefficient|
                                if coefficient["name"] == team1_page_name
                                  Coefficient.create({
                                    title: '1',
                                    value: coefficient["oddsdecimal"],
                                    event_type_one_id: event_type_one.id,
                                    bookmaker_id: bookmaker.id
                                  })
                                elsif coefficient["name"] == team2_page_name
                                  Coefficient.create({
                                    title: '2',
                                    value: coefficient["oddsdecimal"],
                                    event_type_one_id: event_type_one.id,
                                    bookmaker_id: bookmaker.id
                                  })
                                elsif coefficient["name"] == "Draw"
                                  Coefficient.create({
                                    title: 'x',
                                    value: coefficient["oddsdecimal"],
                                    event_type_one_id: event_type_one.id,
                                    bookmaker_id: bookmaker.id
                                  })
                                end
                              end
                            end
                          end
                        end
                      end
                    else
                      championship_association_not_found championship_page_name, sport_page_association_name
                    end
                  else
                    championship_association_not_found championship_page_name, sport_page_association_name
                  end
                end
              end
            else
              sport_association_not_found sport_page_association_name
            end
          end
        end
      end
    end

    def event_type_one_already_exists event_obj
      return true if get_event_type_one_by_data event_obj
    end

    def get_event_type_one_by_data event_obj
      return EventTypeOne.find_by_opponent_1_and_opponent_2(event_obj[:opponent_1], event_obj[:opponent_2])
    end

    def get_championship_by_title_and_sport_id title, sport_id
      ChampionshipAssociation.where(title: title).each do |championship_association|
        if championship_association.championship.sport_id == sport_id
          return championship_association.championship
        end
      end
      nil
    end

    def get_sport_page
      http = HTTPClient.new
      http.transparent_gzip_decompression = true
      http.get "http://o5ugi3q.o5uwy3djmfwwq2lmnqxgg33n.cmle.ru/pricefeed/openbet_cdn?action=template"
    end

    def get_events_page_by_sport_id sport_id
      http = HTTPClient.new
      http.transparent_gzip_decompression = true
      http.get "http://o5ugi3q.o5uwy3djmfwwq2lmnqxgg33n.cmle.ru/pricefeed/openbet_cdn?action=template&template=getHierarchyByMarketType&classId=#{sport_id}"
    end

    def git_categories
      [
        'AMERICAN_FB',
        'BASEBALL',
        'BASKETBALL',
        'CRICKET',
        'DARTS',
        'FOOTBALL',
        'ICE_HOCKEY',
        'MOTOR_RACING',
        'MOTORBIKES',
        'OTHER_SPORTS',
        'RUGBY_LEAGUE',
        'RUGBY_UNION',
        'SNOOKER',
        'TENNIS',
        'WINTER_SPORTS'
      ]
    end

    def sport_association_not_found sport_name
      puts "--- sport association #{sport_name} not found ---"
    end

    def championship_association_not_found championship_name, sport_name
      puts "--- championship association #{championship_name} not found ---"
      puts "--- sport: #{sport_name} ---"
    end

    def team_association_not_found team, championship_name, sport_name
      puts "--- team association #{team} now found ---"
      puts "--- championship id #{championship_name} not found ---"
      puts "--- sport: #{sport_name} ---"
    end
  end
end