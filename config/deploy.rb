# config valid only for current version of Capistrano
lock '3.3.5'

set :application, 'betlog'
set :repo_url, 'git@bitbucket.org:incognitorus/betlog.git'


# deploy.rb or stage file (staging.rb, production.rb or else)
set :rvm_type, :user                     # Defaults to: :auto
set :rvm1_ruby_version, '2.1.3'      # Defaults to: 'default'


#set :rvm_custom_path, '~/.myveryownrvm'  # only needed if not detected

# Default branch is :master
# ask :branch, proc { `git rev-parse --abbrev-ref HEAD`.chomp }.call

# Default value for :scm is :git
# set :scm, :git

# Default value for :format is :pretty
# set :format, :pretty

# Default value for :log_level is :debug
# set :log_level, :debug

# Default value for :pty is false
# set :pty, true

# Default value for :linked_files is []
set :linked_files, fetch(:linked_files, []).push('config/database.yml')

# Default value for linked_dirs is []
set :linked_dirs, fetch(:linked_dirs, []).push('bin', 'log', 'tmp/pids', 'tmp/cache', 'tmp/sockets', 'vendor/bundle', 'public/system', 'public/uploads')

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for keep_releases is 5
# set :keep_releases, 5
before 'deploy', 'rvm1:install:rvm'
before 'deploy', 'rvm1:install:ruby'




after 'deploy:publishing', 'deploy:restart'
namespace :deploy do
  task :restart do
    invoke 'unicorn:restart'
  end
end
namespace :deploy do
  task :stop do
    invoke 'unicorn:stop'
  end
end
namespace :deploy do
  task :start do
    invoke 'unicorn:start'
  end
end
