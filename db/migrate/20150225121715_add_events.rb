class AddEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.references :championship, null: false

      t.timestamps
    end
  end
end
