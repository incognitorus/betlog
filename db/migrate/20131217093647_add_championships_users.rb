class AddChampionshipsUsers < ActiveRecord::Migration
  def change
    create_table :championships_users, id: false do |t|
      t.references :championship, null: false
      t.references :user, null: false
    end
  end
end
