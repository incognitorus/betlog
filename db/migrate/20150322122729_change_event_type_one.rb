class ChangeEventTypeOne < ActiveRecord::Migration
  def change
    remove_column :event_type_ones, :opponent_1, :string
    remove_column :event_type_ones, :opponent_2, :string
    add_column :event_type_ones, :opponent_1, :integer
    add_column :event_type_ones, :opponent_2, :integer
  end
end
