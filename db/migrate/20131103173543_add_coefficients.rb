class AddCoefficients < ActiveRecord::Migration
  def change
  	create_table :coefficients do |t|
      t.string :title
      t.float :value

      t.references :bookmaker, null: false
      t.references :event_type_one, null: false

      t.timestamps
    end
  end
end
