class AddBookmakers < ActiveRecord::Migration
  def change
  	create_table :bookmakers do |t|
      t.string :title
      t.float :rating

      t.timestamps
    end
  end
end
