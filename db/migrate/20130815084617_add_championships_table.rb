class AddChampionshipsTable < ActiveRecord::Migration
  def change
  	create_table :championships do |t|
      t.string :title
      t.references :sport, null: false

      t.timestamps
    end
  end
end
