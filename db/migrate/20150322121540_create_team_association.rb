class CreateTeamAssociation < ActiveRecord::Migration
  def change
    create_table :team_associations do |t|
      t.references :team
      t.string :title

      t.timestamps
    end
  end
end
