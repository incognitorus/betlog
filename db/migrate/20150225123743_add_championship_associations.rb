class AddChampionshipAssociations < ActiveRecord::Migration
  def change
    create_table :championship_associations do |t|
      t.string :title

      t.references :championship, null: false
      t.timestamps
    end
  end
end
