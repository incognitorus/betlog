class AddSportAssociation < ActiveRecord::Migration
  def change
    create_table :sport_associations do |t|
      t.string :title

      t.references :sport, null: false
      t.timestamps
    end
  end
end
