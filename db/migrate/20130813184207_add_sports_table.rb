#encoding: utf-8
class AddSportsTable < ActiveRecord::Migration
  def change
  	create_table :sports do |t|
      t.string :title
      t.references :championship
      t.timestamps 
    end
  end
end
