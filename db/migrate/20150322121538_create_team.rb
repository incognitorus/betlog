class CreateTeam < ActiveRecord::Migration
  def change
    create_table :teams do |t|
      t.references :championship
      t.string :title

      t.timestamps
    end
  end
end
