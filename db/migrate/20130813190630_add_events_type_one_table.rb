class AddEventsTypeOneTable < ActiveRecord::Migration
  def change
  	create_table :event_type_ones do |t|
      t.references :event, null: false
      t.string :opponent_1
      t.string :opponent_2
      t.datetime :date_event

      t.timestamps
    end
  end
end
